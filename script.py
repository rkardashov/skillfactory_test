###############################################################################

import json, requests, mysql.connector

DB_USER = 'username'
DB_PASSWORD = 'password'
DB_HOST = 'localhost'
DB_NAME = 'test'

def connect_to_db():
    return mysql.connector.connect( \
        user=DB_USER, 
        password=DB_PASSWORD,
        host=DB_HOST,
        database=DB_NAME
    )

def create_db_table(db):
    cursor = db.cursor()
    cursor.execute(f"DROP TABLE IF EXISTS {DB_NAME}.courses")
    sql = f"""CREATE TABLE {DB_NAME}.courses (
        id CHAR(120) PRIMARY KEY,
        id_parent CHAR(120),
        block_id CHAR(32) NOT NULL,
        display_name VARCHAR(500) CHARACTER SET utf8,
        lms_web_url VARCHAR(250),
        student_view_url VARCHAR(250),
        type VARCHAR(10),
        added_at DATETIME DEFAULT CURRENT_TIMESTAMP
        )"""
    cursor.execute(sql)

def add_course_to_db(db, course, parent_id):
    parent_id = f'"{parent_id}"' if parent_id else 'null'
    cursor = db.cursor()
    sql = f"""INSERT INTO {DB_NAME}.courses 
        (id, id_parent, block_id, display_name, lms_web_url, student_view_url, type)
        VALUES(
            "{course['id']}", 
            {parent_id},
            "{course['block_id']}",
            "{course['display_name']}",
            "{course['lms_web_url']}",
            "{course['student_view_url']}",
            "{course['type']}"
        )"""
    cursor.execute(sql)
    db.commit()


###############################################################################
##    entry point
###############################################################################

def do_work():
    api_url = "http://analytics.skillfactory.ru:5000/api/v1.0.1/get_structure_course/"
    response = requests.post(api_url)
    response.raise_for_status()
    data = response.json()

    # with open("data.json") as f:
    #     data = json.loads(f.read())

    db = connect_to_db()
    
    create_db_table(db)

    root_id = data["root"]

    parents = {} # child_id: parent_id
    for block_id, block in data["blocks"].items():
        if block_id != root_id:
            for child_id in block.get("children", []):
                parents[child_id] = block_id

    for i, (block_id, block) in enumerate(data["blocks"].items()):
        if block_id == root_id:
            continue
        print(f"{i+1}. {block['display_name']}")
        add_course_to_db(db, block, parents.get(block_id, None))
        for j, child_id in enumerate(block.get("children", [])):
                child = data["blocks"][child_id]
                print(f"  {i+1}.{j+1}. {child['display_name']}")

    db.close()

if __name__ == "__main__":
    do_work()

