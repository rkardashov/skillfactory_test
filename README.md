
1. Склонировать этот репозиторий
1. Создать и активировать виртуальное окружение Python:  
    ```$ python3 -m venv venv_name```  
    ```$ source venv_name/bin/activate```
1. Установить зависимости:  
    ```$ pip install -r requirements.txt ```
1. Указать параметры подключения к базе данных в скрипте:  
    - ```DB_USER```
    - ```DB_PASSWORD```
    - ```DB_HOST```
    - ```DB_NAME```
1. Запустить скрипт:  
```$ python script.py```
1. PROFIT
